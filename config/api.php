<?php

return [

    'scopes' => [
      'openid'  => 'Open ID Connect',
      'profile' => 'Open ID Connect profile scope',
      'email'   => 'Open ID Connect email scope',
      'other'   => 'An other scope (test)',
    ]
];
