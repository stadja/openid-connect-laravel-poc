<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Passport\Passport;

use App\User;
use Laravel\Passport\Client;
use Defuse\Crypto\Crypto;

class OauthTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Check if API respond to not connected
     *
     * @return void
     */
    public function testNotConnectedCall()
    {
        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])->json('GET', '/oauth/token');
        $response->assertStatus(405);

        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])->json('POST', '/oauth/token');
        $response->assertStatus(400);

        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])->json('GET', '/api/userinfo');
        $response->assertStatus(401);
    }

    public function testCreateAccessToken()
    {
        $client = factory(Client::class)->create();
        $user = factory(User::class)->create();

        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])->json('POST', '/oauth/token', [
            "client_id" => $client->id,
            "client_secret" => $client->secret,
            "username" => $user->email,
            "password" => "wrong password",
            "scope" => "",
            "grant_type" => "password"
        ]);
        $response->assertJsonMissing([
            'access_token' => true,
            'refresh_token' => true,
            'id_token' => true,
        ]);
        $response->assertStatus(401);

        $response = $this->json('POST', '/oauth/token', [
            "client_id" => $client->id,
            "client_secret" => $client->secret,
            "username" => $user->email,
            "password" => "password",
            "scope" => "",
            "grant_type" => "password"
        ]);
        $response->assertJson([
            'access_token' => true,
            'refresh_token' => true,
            'id_token' => true,
        ]);
        $response->assertStatus(200);

        $response = $this->json('POST', '/oauth/token', [
            "client_id" => $client->id,
            "client_secret" => $client->secret,
            "username" => $user->email,
            "password" => "password",
            "scope" => "profile",
            "grant_type" => "password"
        ]);
        $response->assertJson([
            'access_token' => true,
            'refresh_token' => true,
        ]);
        $response->assertJsonMissing([
            'id_token' => true,
        ]);
        $response->assertStatus(200);
    }

    public function testRefreshToken()
    {
        $client = factory(Client::class)->create();
        $user = factory(User::class)->create();

        $response = $this->json('POST', '/oauth/token', [
            "client_id" => $client->id,
            "client_secret" => $client->secret,
            "username" => $user->email,
            "password" => "password",
            "scope" => "",
            "grant_type" => "password"
        ]);

        $response = $response->content();
        $response = json_decode($response);
        $refreshToken = $response->refresh_token;

        $response = $this->json('POST', '/oauth/token', [
            "client_id" => $client->id,
            "client_secret" => $client->secret,
            "grant_type" => "refresh_token",
            "refresh_token" => "WRONG REFRESH TOKEN"
        ]);
        $response->assertStatus(401);

        $response = $this->json('POST', '/oauth/token', [
            "client_id" => $client->id,
            "client_secret" => $client->secret,
            "grant_type" => "refresh_token",
            "refresh_token" => $refreshToken
        ]);
        $response->assertStatus(200);

        $response = $response->content();
        $response = json_decode($response);
        $newRefreshToken = $response->refresh_token;

        $this->assertTrue($refreshToken != $newRefreshToken);

        $refreshToken = $newRefreshToken;
        $decrypt = Crypto::decryptWithPassword($refreshToken, app('encrypter')->getKey());
        $decrypt = json_decode($decrypt);

        $response = $this->json('POST', '/oauth/token', [
            "client_id" => $client->id,
            "client_secret" => $client->secret,
            "grant_type" => "refresh_token",
            "refresh_token" => $decrypt->access_token_id
        ]);
        $response->assertStatus(401);

        $response = $this->json('POST', '/oauth/token', [
            "client_id" => $client->id,
            "client_secret" => $client->secret,
            "grant_type" => "refresh_token",
            "refresh_token" => $decrypt->refresh_token_id
        ]);
        $response->assertStatus(200);
        $response->assertJson([
            'access_token' => true,
            'refresh_token' => true,
            'id_token' => true,
        ]);
    }

    public function testIdToken()
    {
        $client1 = factory(Client::class)->create();
        $client2 = factory(Client::class)->create();
        $user = factory(User::class)->create();

        $response = $this->json('POST', '/oauth/token', [
            "client_id" => $client1->id,
            "client_secret" => $client1->secret,
            "username" => $user->email,
            "password" => "password",
            "scope" => "",
            "grant_type" => "password"
        ]);

        $response = $response->content();
        $response = json_decode($response);
        $idToken = $response->id_token;

        $response = $this->json('POST', '/oauth/token', [
            "client_id" => $client1->id,
            "client_secret" => $client1->secret,
            "grant_type" => "id_token",
            "id_token" => "WRONG REFRESH TOKEN"
        ]);
        $response->assertStatus(401);

        $response = $this->json('POST', '/oauth/token', [
            "client_id" => $client2->id,
            "client_secret" => $client2->secret,
            "grant_type" => "id_token",
            "id_token" => "WRONG REFRESH TOKEN"
        ]);
        $response->assertStatus(401);

        $response = $this->json('POST', '/oauth/token', [
            "client_id" => $client1->id,
            "client_secret" => $client1->secret,
            "grant_type" => "id_token",
            "id_token" => $idToken
        ]);
        $response->assertStatus(200);
        $response->assertJson([
            'access_token' => true,
            'refresh_token' => true,
            'id_token' => true,
        ]);
        $response = $response->content();
        $response = json_decode($response);
        $accessToken1 = $response->access_token;

        $response = $this->json('POST', '/oauth/token', [
            "client_id" => $client2->id,
            "client_secret" => $client2->secret,
            "grant_type" => "id_token",
            "id_token" => $idToken
        ]);
        $response->assertStatus(200);
        $response->assertJson([
            'access_token' => true,
            'refresh_token' => true,
            'id_token' => true,
        ]);
        $response = $response->content();
        $response = json_decode($response);
        $accessToken2 = $response->access_token;

        $this->assertTrue($accessToken1 != $accessToken2);
    }

    public function testGetUserInfo()
    {
        Passport::actingAs(
            factory(User::class)->create(),
            ['other']
        );
        $response = $this->get('/api/userinfo');
        $response->assertStatus(403);

        Passport::actingAs(
            factory(User::class)->create(),
            ['openid', 'profile', 'email']
        );
        $response = $this->get('/api/userinfo');
        $response->assertStatus(200);
        $response->assertJson([
            'email' => true,
            'name' => true,
        ]);

        Passport::actingAs(
            factory(User::class)->create(),
            ['openid', 'profile']
        );
        $response = $this->get('/api/userinfo');
        $response->assertStatus(200);
        $response->assertJson([
            'name' => true,
        ]);
        $response->assertJsonMissing([
            'email' => true
        ]);

        Passport::actingAs(
            factory(User::class)->create(),
            ['openid']
        );
        $response = $this->get('/api/userinfo');
        $response->assertStatus(200);
        $response->assertJsonMissing([
            'name' => true,
            'email' => true
        ]);
    }
}
