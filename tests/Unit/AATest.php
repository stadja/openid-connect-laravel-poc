<?php

namespace Tests\Unit;

use Tests\TestCase;

class AATest extends TestCase
{

    /**
     * Check if API respond to not connected
     *
     * @return void
     */
    public function testCheckEnvIsOk()
    {
        $this->assertTrue(env('APP_ENV') == 'testing', 'APP_ENV => testing');
        $this->assertTrue(env('LOG_CHANNEL') == 'testing', 'LOG_CHANNEL => testing');
    }
}
