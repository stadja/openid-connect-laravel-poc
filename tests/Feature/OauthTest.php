<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

use App\User;
use Laravel\Passport\Client;

class OauthTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testFromNothingToUserinfo()
    {
        $client1 = factory(Client::class)->create();
        $client2 = factory(Client::class)->create();
        $user = factory(User::class)->create();

        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])->get('/api/userinfo');
        $response->assertStatus(401);

        $response = $this->json('POST', '/oauth/token', [
            "client_id" => $client1->id,
            "client_secret" => $client1->secret,
            "username" => $user->email,
            "password" => "password",
            "scope" => "openid profile email",
            "grant_type" => "password"
        ]);
        $response->assertJson([
            'access_token' => true,
            'refresh_token' => true,
            'id_token' => true,
        ]);
        $response->assertStatus(200);

        $response = $response->content();
        $response = json_decode($response);
        $refreshToken = $response->refresh_token;
        $accessToken = $response->access_token;

        $response = $this->json('POST', '/oauth/token', [
            "client_id" => $client1->id,
            "client_secret" => $client1->secret,
            "grant_type" => "refresh_token",
            "refresh_token" => "WRONG TOKEN"
        ]);
        $response->assertStatus(401);

        $response = $this->json('POST', '/oauth/token', [
            "client_id" => $client1->id,
            "client_secret" => $client1->secret,
            "grant_type" => "refresh_token",
            "refresh_token" => $refreshToken
        ]);
        $response->assertStatus(200);

        $response = $response->content();
        $response = json_decode($response);
        $idToken = $response->id_token;

        $response = $this->json('POST', '/oauth/token', [
            "client_id" => $client2->id,
            "client_secret" => $client2->secret,
            "grant_type" => "id_token",
            "id_token" => "WRONG TOKEN"
        ]);
        $response->assertStatus(401);

        $response = $this->json('POST', '/oauth/token', [
            "client_id" => $client2->id,
            "client_secret" => $client2->secret,
            "grant_type" => "id_token",
            "id_token" => $idToken
        ]);
        $response->assertStatus(200);

        $response = $response->content();
        $response = json_decode($response);
        $newAccessToken = $response->access_token;

        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'Authorization' => 'Bearer '.$accessToken,
        ])->get('/api/userinfo');
        $response->assertStatus(401);

        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'Authorization' => 'Bearer '.$newAccessToken,
        ])->get('/api/userinfo');
        $response->assertStatus(200);
        $response->assertJson([
            'id' => true,
            'email' => true,
        ]);
    }
}

