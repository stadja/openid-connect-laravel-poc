<?php
/**
 * @author Steve Rhoades <sedonami@gmail.com>
 * @license http://opensource.org/licenses/MIT MIT
 */
namespace App\Passport;

use OpenIDConnectServer\IdTokenResponse as BaseIdTokenResponse;

use League\OAuth2\Server\Entities\UserEntityInterface;
use League\OAuth2\Server\Entities\AccessTokenEntityInterface;
use Lcobucci\JWT\Builder;


class IdTokenResponse extends BaseIdTokenResponse
{
    protected $builder;

    protected function getBuilder(AccessTokenEntityInterface $accessToken, UserEntityInterface $userEntity)
    {
        // Add required id_token claims
        $this->builder = (new Builder())
            ->setAudience($accessToken->getClient()->getIdentifier())
            ->setIssuer(config('app.url'))
            ->setIssuedAt(time())
            ->setExpiration($accessToken->getExpiryDateTime()->getTimestamp())
            ->setSubject($userEntity->getIdentifier());

        return $this->builder;
    }

    public function getInitialisedBuilder()
    {
        return $this->builder;
    }
}
