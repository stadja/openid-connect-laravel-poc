<?php

namespace App\Passport\Bridge;

use  Laravel\Passport\Bridge\RefreshTokenRepository as PassportRefreshTokenRepository;

class RefreshTokenRepository extends PassportRefreshTokenRepository
{

    /**
     * {@inheritdoc}
     */
    public function find($tokenId)
    {
        $refreshToken = $this->database->table('oauth_refresh_tokens')
                    ->where('id', $tokenId)->first();

        return $refreshToken;
    }
}
