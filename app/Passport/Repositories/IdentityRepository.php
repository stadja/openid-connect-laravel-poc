<?php
namespace App\Passport\Repositories;

use \OpenIDConnectServer\Repositories\IdentityProviderInterface;
use \Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Support\Facades\Auth;

class IdentityRepository implements IdentityProviderInterface {
  public function getUserEntityByIdentifier($identifier) {

    $test = Auth::createUserProvider('users');
    $user = $test->retrieveById($identifier);

    return $user;
  }
}
