<?php
/**
 * OAuth 2.0 Refresh token grant.
 *
 * @author      Alex Bilbie <hello@alexbilbie.com>
 * @copyright   Copyright (c) Alex Bilbie
 * @license     http://mit-license.org/
 *
 * @link        https://github.com/thephpleague/oauth2-server
 */

namespace App\Passport\Grant;

use DateInterval;
use Exception;
use League\OAuth2\Server\Exception\OAuthServerException;
use League\OAuth2\Server\Repositories\RefreshTokenRepositoryInterface;
use League\OAuth2\Server\RequestEvent;
use League\OAuth2\Server\ResponseTypes\ResponseTypeInterface;
use Psr\Http\Message\ServerRequestInterface;

use League\OAuth2\Server\Grant\AbstractGrant;
use DateTime;
use League\OAuth2\Server\CryptKey;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\ValidationData;
use Lcobucci\JWT\Signer\Key;
use Lcobucci\JWT\Signer\Rsa\Sha256;
use App\Exceptions\OidcException;

use App\User;

/**
 * Id token grant.
 */
class IdTokenGrant extends AbstractGrant
{
    /**
     * @var DateInterval
     */
    private $idTokenTTL;

    /**
     * @var League\OAuth2\Server\CryptKey
     */
    protected $publicKey;

    /**
     * @var League\OAuth2\Server\CryptKey
     */
    protected $privateKey;

    public function __construct(RefreshTokenRepositoryInterface $refreshTokenRepository,DateInterval $idTokenTTL, CryptKey $publicKey, CryptKey $privateKey)
    {
        $this->setRefreshTokenRepository($refreshTokenRepository);
        $this->idTokenTTL = $idTokenTTL;
        $this->publicKey  = $publicKey;
        $this->privateKey = $privateKey;
    }

    /**
     * {@inheritdoc}
     */
    public function respondToAccessTokenRequest(
        ServerRequestInterface $request,
        ResponseTypeInterface $responseType,
        DateInterval $accessTokenTTL
    ) {
        $client = $this->validateClient($request);

        $token = $this->getRequestParameter('id_token', $request);
        $validatedToken = $this->validateToken($token, $this->publicKey, $this->privateKey);
        $user = User::find($validatedToken->getClaim('sub'));
        if (!$user) {
            throw OidcException::invalidIdToken('user not found');
        }

        $scopes = $this->validateScopes($this->getRequestParameter(
            'scope',
            $request,
            implode(self::SCOPE_DELIMITER_STRING, array('openid')))
        );

        // Issue and persist new access token
        $accessToken = $this->issueAccessToken($accessTokenTTL, $client, $user->id, $scopes);
        $this->getEmitter()->emit(new RequestEvent(RequestEvent::ACCESS_TOKEN_ISSUED, $request));
        $responseType->setAccessToken($accessToken);

        // Issue and persist new refresh token if given
        $refreshToken = $this->issueRefreshToken($accessToken);

        if ($refreshToken !== null) {
            $this->getEmitter()->emit(new RequestEvent(RequestEvent::REFRESH_TOKEN_ISSUED, $request));
            $responseType->setRefreshToken($refreshToken);
        }

        return $responseType;
    }

    public function validateToken($token, $publicKey, $privateKey) {

        try {
            $token = (new Parser())->parse((string) $token); // Parses from a string
        } catch(\Exception $e) {
            throw OidcException::invalidIdToken('error while parsing id_token');
        }

        $data = new ValidationData(); // It will use the current time to validate (iat, nbf and exp)
        $data->setIssuer(config('app.url'));

        if (!$token->validate($data)) {
            throw OidcException::invalidIdToken('cannot validate id_token');
        }

        if (!$token->verify(new Sha256(), new Key($publicKey->getKeyPath()))) {
            throw OidcException::invalidIdToken('cannot verify id_token');
        }

        return $token;
    }

    /**
     * {@inheritdoc}
     */
    public function getIdentifier()
    {
        return 'id_token';
    }
}
