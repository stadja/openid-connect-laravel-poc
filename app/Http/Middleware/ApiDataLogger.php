<?php
namespace App\Http\Middleware;
use Closure;
use Lcobucci\JWT\Parser;
use Illuminate\Support\Facades\Log;

class ApiDataLogger
{
  protected $toFilter = ["password", "client_secret"];
  private $startTime;

/**
* Handle an incoming request.
*
* @param  \Illuminate\Http\Request  $request
* @param  \Closure  $next
* @return mixed
*/

public function handle($request, Closure $next)
{
  $this->startTime = microtime(true);
  return $next($request);
}
public function terminate($request, $response)
{
  if ( env('API_DATALOGGER', true) ) {
    $dataToLog  = 'Time: '   . time() . "\n";
    if(defined('LARAVEL_START')) {
      $endTime = microtime(true);
      $dataToLog .= 'Duration: ' . number_format($endTime - LARAVEL_START, 3) . "\n";
    }
    $dataToLog .= 'IP Address: ' . $request->ip() . "\n";
    $dataToLog .= 'URL: '    . $request->fullUrl() . "\n";
    $dataToLog .= 'Method: ' . $request->method() . "\n";
    $dataToLog .= 'Input: '  . $this->filterContent($request->getContent()) . "\n";
    $dataToLog .= 'Output: ' . $this->filterContent($response->getContent()) . "\n";
    if ($request->user()) {
      $dataToLog .= 'User: ' . $request->user()->getIdentifier() . "\n";
    } else {
      $content = $response->getContent();
      $content = json_decode($content);
      if ($content && isset($content->id_token)) {
        try {
          $token = (new Parser())->parse((string) $content->id_token);
          if ($token->hasClaim('sub')) {
            $dataToLog .= 'User: ' . $token->getClaim('sub') . "\n";
          }
        } catch(\Exception $e) {}
      }
    }
    Log::channel('stats')->info($dataToLog);

    // \File::append( storage_path('logs' . DIRECTORY_SEPARATOR . $filename), $dataToLog . "\n" . str_repeat("=", 20) . "\n\n");
  }
}

private function filterContent($content) {
  $temp = json_decode($content);
  if (!$temp) {
    return $content;
  }
  $content = $temp;
  foreach($this->toFilter as $filter) {
    if (isset($content->$filter)) {
      $content->$filter = "********";
    }
  }
  return json_encode($content);
}
}
