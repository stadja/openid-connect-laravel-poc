<?php

namespace App\Http\Middleware;
use Psr\Http\Message\ServerRequestInterface;
use \League\OAuth2\Server\AuthorizationServer;
use \League\OAuth2\Server\CryptKey;
use Defuse\Crypto\Crypto;
use Defuse\Crypto\Key;
use App\Passport\Bridge\RefreshTokenRepository;
use Laravel\Passport\Bridge\RefreshToken;
use Laravel\Passport\TokenRepository;

use Closure;

class CheckOldRefreshToken
{
    /**
     * The database connection.
     *
     * @var \Illuminate\Database\Connection
     */
    protected $database;

    /**
     * @var string|Key
     */
    protected $encryptionKey;

    /**
     * @var \League\OAuth2\Server\AuthorizationServer
     */
    protected $authorizationServer;

    /**
     * @var Laravel\Passport\TokenRepository
     */
    protected $tokenRepository;

    public function __construct(AuthorizationServer $authorizationServer, TokenRepository $tokenRepository) {
        $this->authorizationServer = $authorizationServer;
        $this->tokenRepository = $tokenRepository;

    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $grant_type = $request->get('grant_type');
        // if you don't want to refresh a token, next middleware
        if ($grant_type != 'refresh_token') {
            return $next($request);
        }

        $refreshToken = $request->get('refresh_token');
        // if you don't pass a refresh a token, next middleware
        if (!$refreshToken) {
            return $next($request);
        }

        $encrypter = app('encrypter')->getKey();

        try {
            if ($encrypter instanceof Key) {
                $decrypt = Crypto::decrypt($refreshToken, $encrypter);
            } else {
                $decrypt = Crypto::decryptWithPassword($refreshToken, $encrypter);
            }

            // if you can decrypt the refresh_token, next middleware
            return $next($request);

        } catch (\Exception $e) {}


        // We have a refresh_token, we can't decrypt it. Let's check if it exists
        $repository = app(RefreshTokenRepository::class);
        $isRevoked = $repository->isRefreshTokenRevoked($refreshToken);
        if ($isRevoked) {

            // if you can't find the refresh_token or it is revoked, next middleware
            return $next($request);
        }

        $refreshToken = $repository->find($refreshToken);
        $accessToken = $this->tokenRepository->find($refreshToken->access_token_id);

        $refreshToken = json_encode(
            [
                'client_id'        => (string)$accessToken->client_id,
                'refresh_token_id' => (string)$refreshToken->id,
                'access_token_id'  => (string)$accessToken->id,
                'scopes'           => $accessToken->scopes,
                'user_id'          => (string)$accessToken->user_id,
                'expire_time'      => strtotime($refreshToken->expires_at)
            ]
        );

        if ($encrypter instanceof Key) {
            $encrypt = Crypto::encrypt($refreshToken, $encrypter);
        } else {
            $encrypt =  Crypto::encryptWithPassword($refreshToken, $encrypter);
        }

        $inputs = $request->all();
        $inputs['refresh_token'] = $encrypt;
        $request->replace($inputs);


        // $refreshToken = $repository->getNewRefreshToken();
        // var_dump(bin2hex(random_bytes(40)));

        return $next($request);
    }
}
