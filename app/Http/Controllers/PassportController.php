<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use \OpenIDConnectServer\ClaimExtractor;

class PassportController extends Controller
{
    protected $claimExtractor;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ClaimExtractor $claimExtractor)
    {
        $this->claimExtractor = $claimExtractor;
    }



    /**
     * @OA\Get(
     *     path="/api/userinfo",
     *     operationId="userinfo",
     *     tags={"OpenId Connect"},
     *     summary="Get authentified user info",
     *     description="Get authentified user infos in relation of token scopes",
     *     @OA\Response(
     *         response=200,
     *         description="successful operation",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *         )
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthenticated"
     *     ),
     *     security={
     *         {"passport": {"openid"}}
     *     }
     * )
     *
     * Returns user info
     */
    public function userInfo(Request $request)
    {
        $user = $request->user();
        $token = $user->token();

        $scopes = [];
        $existingScopes = config('api.scopes');
        foreach ($existingScopes as $scopeToTest => $description) {
            if ($token->can($scopeToTest)) {
                $scopes[] = $scopeToTest;
            }
        }
        $userInfo = $this->claimExtractor->extract($scopes, $user->getClaims());

        return $userInfo;
    }
}
