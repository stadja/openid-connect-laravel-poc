<?php
namespace App\Providers;

use  Laravel\Passport\Passport;
use App\Passport\Bridge\RefreshTokenRepository;
use App\Passport\Repositories\IdentityRepository;
use \OpenIDConnectServer\ClaimExtractor;
use \OpenIDConnectServer\Entities\ClaimSetEntity;
use \OpenIDConnectServer\IdTokenResponse;
use App\Passport\IdTokenResponse as MyIdTokenResponse;
use League\OAuth2\Server\AuthorizationServer;
use Laravel\Passport\Bridge\PersonalAccessGrant;
use League\OAuth2\Server\Grant\ClientCredentialsGrant;
use App\Passport\Grant\IdTokenGrant;

class PassportServiceProvider extends \Laravel\Passport\PassportServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        parent::register();

        $this->app->singleton(ClaimExtractor::class, function () {
            return $this->makeClaimExtractor();
        });
        $this->app->singleton(IdTokenResponse::class, function () {
            return $this->makeIdTokenResponse();
        });
    }

    /**
     * Register the authorization server.
     *
     * @return void
     */
    protected function registerAuthorizationServer()
    {
        $this->app->singleton(AuthorizationServer::class, function () {
            return tap($this->makeAuthorizationServer(), function ($server) {
                $server->setDefaultScope(Passport::$defaultScope);

                $server->enableGrantType(
                    $this->makeAuthCodeGrant(), Passport::tokensExpireIn()
                );

                $server->enableGrantType(
                    $this->makeRefreshTokenGrant(), Passport::tokensExpireIn()
                );

                $server->enableGrantType(
                    $this->makePasswordGrant(), Passport::tokensExpireIn()
                );

                $server->enableGrantType(
                    new PersonalAccessGrant, Passport::personalAccessTokensExpireIn()
                );

                $server->enableGrantType(
                    new ClientCredentialsGrant, Passport::tokensExpireIn()
                );

                if (Passport::$implicitGrantEnabled) {
                    $server->enableGrantType(
                        $this->makeImplicitGrant(), Passport::tokensExpireIn()
                    );
                }

                $server->enableGrantType(
                    $this->makeIdTokenGrant(), Passport::tokensExpireIn()
                );
            });
        });
    }

    /**
     * Create and configure a Facebook grant instance.
     *
     * @return FacebookGrant
     */
    protected function makeIdTokenGrant()
    {
        $grant = new IdTokenGrant(
            $this->app->make(RefreshTokenRepository::class),
            Passport::tokensExpireIn(),
            $this->makeCryptKey('public'),
            $this->makeCryptKey('private')
        );

        $grant->setRefreshTokenTTL(Passport::refreshTokensExpireIn());

        return $grant;
    }

    public function makeClaimExtractor() {
        $extractor = new ClaimExtractor();
        $claimSet = new ClaimSetEntity('openid', [
            'id',
            'email'
        ]);
        $extractor->addClaimSet($claimSet);

        return $extractor;
    }

    public function makeIdTokenResponse() {

        return new MyIdTokenResponse(new IdentityRepository(), $this->makeClaimExtractor());
    }

    /**
     * Make the authorization service instance.
     *
     * @return \League\OAuth2\Server\AuthorizationServer
     */
    public function makeAuthorizationServer()
    {
      return new AuthorizationServer(
        $this->app->make(\Laravel\Passport\Bridge\ClientRepository::class),
        $this->app->make(\Laravel\Passport\Bridge\AccessTokenRepository::class),
        $this->app->make(\Laravel\Passport\Bridge\ScopeRepository::class),
        $this->makeCryptKey('private'),
        app('encrypter')->getKey(),
        $this->makeIdTokenResponse()
    );
  }
}
