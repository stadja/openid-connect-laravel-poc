<?php

namespace App\Providers;

use Laravel\Passport\Passport;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use App\Passport\Models\Client;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];


    /**
     *    @OA\Post(
     *        path="/oauth/token",
     *        operationId="postToken",
     *        tags={"oAuth 2"},
     *        summary="Get Access Token | Refresh the Refresh Token",
     *        description="Get Access Token or refresh Refresh Token for oAuth2 protocol",
     *        @OA\Response(
     *            response=200,
     *            description="successful operation",
     *            @OA\MediaType(
     *                mediaType="application/json",
     *                )
     *            ),
     *        @OA\Response(
     *            response=400,
     *            description="Bad Request"
     *            ),
     *        @OA\Response(
     *            response=401,
     *            description="Unauthorized"
     *            ),
     *        @OA\RequestBody(
     *            description="Input data format",
     *            @OA\MediaType(
     *                mediaType="application/json",
     *                @OA\Schema(
     *                    type="object",
     *                    example={
     *                        "client_id": "x",
     *                        "client_secret": "xxxxxx",
     *                        "password": "xxxx",
     *                        "username": "xxxx@xxx.cxxxxxom",
     *                        "scope": "openid profile email",
     *                        "grant_type": "password"
     *                    },
     *                    required={"client_id", "client_secret", "client_secret", "grant_type"},
     *                    properties={
     *                        @OA\Property(
     *                            type="string",
     *                            property="client_id",
     *                            description="The client Id",
     *                            ),
     *                        @OA\Property(
     *                            type="string",
     *                            property="client_secret",
     *                            description="The client secret",
     *                            ),
     *                        @OA\Property(
     *                            type="string",
     *                            property="grant_type",
     *                            description="Protocol of authorization",
     *                            enum={"password", "refresh_token"}
     *                            ),
     *                        @OA\Property(
     *                            type="string",
     *                            property="username",
     *                            description="Authorizating user email if grant type is password",
     *                            ),
     *                        @OA\Property(
     *                            type="string",
     *                            property="password",
     *                            description="Authorizating user password if grant type is password",
     *                            ),
     *                        @OA\Property(
     *                            property="scope",
     *                            description="The scope to be set to the access_token if grant type is password",
     *                            collectionFormat="ssv",
     *                            type="array",
     *                            default="openid",
     *                            @OA\Items(
     *                                type="string",
     *                                enum = {"opendid", "email", "profile"},
     *                                )
     *                            ),
     *                        @OA\Property(
     *                            type="string",
     *                            property="refresh_token",
     *                            description="Refreshing token if grant type is refresh_token",
     *                            ),
     *                        @OA\Property(
     *                            type="string",
     *                            property="id_token",
     *                            description="Id token if grant type is id_token",
     *                            ),
     *                        }
     *                    )
     *                )
     *            )
     *        )
     *
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Passport::routes(null, ['middleware' => ['checkOldRefreshToken', 'apiLogger']]);

        // Passport::enableImplicitGrant();

        Passport::useClientModel(Client::class);
        Passport::tokensExpireIn(now()->addHours(1));
        Passport::refreshTokensExpireIn(now()->addDays(2));
        Passport::personalAccessTokensExpireIn(now()->addMonths(6));
        Passport::cookie('test_oauth_server');
        Passport::tokensCan(config('api.scopes'));

        Passport::setDefaultScope([
          'openid'
        ]);
    }
}
