<?php
/**
 * @author      Alex Bilbie <hello@alexbilbie.com>
 * @copyright   Copyright (c) Alex Bilbie
 * @license     http://mit-license.org/
 *
 * @link        https://github.com/thephpleague/oauth2-server
 */

namespace App\Exceptions;

use Exception;
use Psr\Http\Message\ResponseInterface;
use Throwable;
use League\OAuth2\Server\Exception\OAuthServerException;

class OidcException extends OAuthServerException
{
    /**
     * Invalid id token.
     *
     * @param null|string $hint
     * @param Throwable   $previous
     *
     * @return static
     */
    public static function invalidIdToken($hint = null, Throwable $previous = null)
    {
        return new static('The id token is invalid.', 1, 'invalid_request', 401, $hint, null, $previous);
    }
}
