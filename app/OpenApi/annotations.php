<?php

/**
 * @OA\Info(
 *      version="1.0.0",
 *      title="Authentification Micro Server",
 *      description="API des différents sujets d'authentification",
 *      @OA\Contact(
 *          email="joel@bobelweb.eu"
 *      ),
 *     @OA\License(
 *         name="Apache 2.0",
 *         url="http://www.apache.org/licenses/LICENSE-2.0.html"
 *     )
 * )
 *
 *  @OA\Server(
 *      url=L5_SWAGGER_CONST_HOST,
 *      description="Authentification Micro Server host server"
 *  )
 */
