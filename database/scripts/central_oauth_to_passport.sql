-- 0) clean everything
TRUNCATE oauth_refresh_tokens;
TRUNCATE oauth_access_tokens;
TRUNCATE oauth_clients;
DELETE FROM users WHERE id > 1;

-- 1) transfer user central_oauth.oauth_users in users
INSERT INTO users (
    SELECT NULL as id, last_name as name, username as email, NULL as email_verified_at, password, NULL as remember_token, CURRENT_TIMESTAMP as created_at, CURRENT_TIMESTAMP as updated_at
    FROM central_oauth.oauth_users
);

-- 2) transfer user central_oauth.oauth_clients into oauth_clients
INSERT INTO oauth_clients (
    SELECT NULL as id, NULL as user_id, client_id as name, client_secret as secret, redirect_uri as redirect, 1 as personal_access_client, 1 as password_client, 0 as revoked, CURRENT_TIMESTAMP as created_at, CURRENT_TIMESTAMP as updated_at
    FROM central_oauth.oauth_clients
);

-- 3) Transfer the central_oauth.oauth_access_tokens into oauth_access_tokens
INSERT INTO oauth_access_tokens(
    SELECT access_token AS id,
        users.id as user_id,
        oauth_clients.id as client_id,
        NULL AS name,
        "[\"openid\", \"*\"]" AS scope,
        0 AS revoked,
        CURRENT_TIMESTAMP AS created_at,
        CURRENT_TIMESTAMP AS updated_at,
        expires AS expires_at
    FROM central_oauth.oauth_access_tokens
    LEFT JOIN users on central_oauth.oauth_access_tokens.user_id = users.email
    LEFT JOIN oauth_clients on central_oauth.oauth_access_tokens.client_id = oauth_clients.name
    -- WHERE expires >= 'NOW()'
    WHERE `expires` >= '2019-06-09 00:00:00'
);

-- 4) Transfer the central_oauth.oauth_refresh_tokens into oauth_refresh_tokens
INSERT INTO oauth_refresh_tokens (
    SELECT * FROM (
        SELECT
            refresh_token as id,
            (SELECT id
                FROM oauth_access_tokens
                WHERE oauth_access_tokens.user_id = users.id
                AND oauth_access_tokens.client_id = oauth_clients.id
                AND id IS NOT NULL
            ORDER BY oauth_access_tokens.expires_at DESC
            LIMIT 1) as access_token_id,
            0 as revoked,
            expires AS expires_at
        FROM central_oauth.oauth_refresh_tokens
        LEFT JOIN oauth_clients on central_oauth.oauth_refresh_tokens.client_id = oauth_clients.name
        LEFT JOIN users on central_oauth.oauth_refresh_tokens.user_id = users.email
        WHERE `expires` >= '2019-06-09 00:00:00'
        -- AND expires >= 'NOW()'
    ) as wrapper WHERE access_token_id IS NOT NULL
)
