<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use Laravel\Passport\Client;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Client::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'secret' => '',
        'redirect' => '/',
        'personal_access_client' => '1',
        'password_client' => '1',
        'revoked' => '0',
    ];
});
